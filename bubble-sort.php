<div>
    <ul>
        <li><a href="/merge-sort.php">Merge sort</a></li>
        <li><a href="/bubble-sort.php">Bubble sort</a></li>
        <li><a href="/index.php">Linear search</a></li>
    </ul>
</div>
<?php

echo '<h2>Bubble sort</h2>';

?>
    <form action="bubble-sort.php" method="post">
        <input name="bubble_sort" type="text">
        <input type="submit" value="SUBMIT">
    </form>

<?php
$inputData = $_POST['bubble_sort'];

if (!empty($inputData)) {

    $inputDataArray = explode(",", $inputData);
    $newArray       = bubbleSort($inputDataArray);

    echo 'Input data: ' . $inputData . '<br/><br/>';
    echo 'Output data: ' . implode(',', $newArray);
}

function bubbleSort($inputDataArray)
{
    if (count($inputDataArray) == 1){
        return $inputDataArray;
    }

    $countItem = count($inputDataArray);

    for ($i = 0; $i < $countItem; $i++) {
        for ($k = 0; $k < $countItem - $i - 1; $k++) {
            if ($inputDataArray[$k] > $inputDataArray[$k + 1]){
                $previousItem           = $inputDataArray[$k];
                $inputDataArray[$k]     = $inputDataArray[$k + 1];
                $inputDataArray[$k + 1] = $previousItem;
            }
        }
    }

    return $inputDataArray;
}
?>