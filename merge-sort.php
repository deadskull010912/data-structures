<div>
    <ul>
        <li><a href="/merge-sort.php">Merge sort</a></li>
        <li><a href="/bubble-sort.php">Bubble sort</a></li>
        <li><a href="/index.php">Linear search</a></li>
    </ul>
</div>
<?php

echo '<h2>Merge sort</h2>';

?>
<form action="merge-sort.php" method="post">
    <input name="merge_sort" type="text">
    <input type="submit" value="SUBMIT">
</form>

<?php
$inputData = $_POST['merge_sort'];

if (!empty($inputData)) {
    $inputDataArray = explode(",", $inputData);
    $newArray       = mergeSort($inputDataArray);

    echo 'Input data: ' . $inputData . '<br/><br/>';
    echo 'Output data: ' . implode(',', $newArray);
}

function mergeSort($inputDataArray)
{
    if (count($inputDataArray) == 1){
        return $inputDataArray;
    }

    $middle = count($inputDataArray)/2;
    $left   = array_slice($inputDataArray, 0, $middle);
    $right  = array_slice($inputDataArray, $middle);
    $left   = mergeSort($left);
    $right  = mergeSort($right);

    return mergeArray($left, $right);
}

function mergeArray($left, $right)
{
    $result     = array();
    $leftKey    = 0;
    $rightKey   = 0;
    $countLeft  = count($left);
    $countRight = count($right);

    while ($leftKey < $countLeft && $rightKey < $countRight)
    {
        if ($left[$leftKey] > $right[$rightKey])
        {
            $result[] = trim($right[$rightKey]);
            $rightKey++;
        } else {
            $result[] = trim($left[$leftKey]);
            $leftKey++;
        }
    }
    while ($leftKey < $countLeft)
    {
        $result[] = trim($left[$leftKey]);
        $leftKey++;
    }

    while ($rightKey < $countRight)
    {
        $result[] = trim($right[$rightKey]);
        $rightKey++;
    }
    return $result;
}
?>