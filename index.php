<div>
    <ul>
        <li><a href="/merge-sort.php">Merge sort</a></li>
        <li><a href="/bubble-sort.php">Bubble sort</a></li>
        <li><a href="/index.php">Linear search</a></li>
    </ul>
</div>
<?php

echo '<h2>Linear search</h2>';

?>
<form action="index.php" method="post">
    <label class="label-item">Type text: </label>
    <input name="input_text" class="form-item" type="text">
    <label class="label-item">Search input: </label>
    <input name="liner_search" type="text" class="form-item">
    <input type="submit" value="SUBMIT" class="btn-submit">
</form>

<?php

$inputData  = $_POST['input_text'];
$searchData = $_POST['liner_search'];

if (!empty($inputData) && !empty($searchData)) {
    $inputDataArray = explode(",", $inputData);
    $outputData     = linearSearch($inputDataArray, $searchData);

    echo 'Input data: ' . $inputData . '<br/><br/>';

    if ($outputData == false) {
      echo 'Element is not find.';
    } else {
        echo 'We found element # ' . $outputData;
    }
}

function linearSearch($inputData, $searchData)
{
    foreach ($inputData as $key => $item) {
        if ($item == $searchData) {
            return $key + 1;
        }
    }

    return false;
}
?>
<style>
    .form-item {
        display: block;
        margin: 0 0 20px 20px;
    }
    .label-item {
        display: block;
        margin: 0 0 5px 20px;
    }
    .btn-submit {
        margin: 0 20px;
    }
</style>
