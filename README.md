# Data structures

## - Linear search
## - Bubble sort
## - Merge sort
### Step 1: Clone
#### Clone this git repo and its submodules by running the following commands: git clone

### Step 2: Run application
#### After you have cloned a project, you need to start your local server. And run the application.
#### Run docker-compose up -d

### Step 3: Open the application in a web browser http://localhost:8080.
